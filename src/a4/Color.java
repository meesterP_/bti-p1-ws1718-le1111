package a4;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Der enum Color beschreibt die moegliche Farbe eines Items.
 *<br />
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public enum Color {
    BLACK, BLUE, BROWN, CYAN, GOLD, GRAY, GREEN, MAGENTA, ORANGE, PINK, PURPLE, RED, TAN, VIOLET, WHITE, YELLOW;
}//enum
