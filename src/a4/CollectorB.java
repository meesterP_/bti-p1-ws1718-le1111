package a4;

import java.util.Collection;
import java.util.List;
import java.util.Stack;

public class CollectorB implements Collector_I {
    private Stack<Item> stack;
    
    public CollectorB() {
        stack = new Stack<Item>();        
    }

    @Override
    public Collection<Item> process(Item item) {
        stack.push(item);
        if(stack.size() >= 7) {
            Stack<Item> stackToReturn = new Stack<Item>();
            while (!stack.isEmpty()) {
                stackToReturn.push(stack.pop());
            }
            return stackToReturn;
        }
        return null;
    }

    @Override
    public void reset() {
        stack.clear();
    }

}
