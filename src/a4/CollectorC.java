package a4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectorC implements Collector_I {
    private Map<Color, List<Item>> map;
    
    public CollectorC() {
        map = new HashMap<Color, List<Item>>();
    }

    @Override
    public Collection<Item> process(Item item) {
        if (map.containsKey(item.getColor())) {
            map.get(item.getColor()).add(item);
        } else {
            List<Item> list = new ArrayList<Item>();
            list.add(item);
            map.put(item.getColor(), list);
        }
        if (map.get(item.getColor()).size() >= 4) {
            List<Item> listToReturn = map.get(item.getColor());
            map.remove(item.getColor());
            return listToReturn;
        } else {
            return null;            
        }
    }

    @Override
    public void reset() {
        map.clear();
    }

}
