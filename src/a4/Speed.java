package a4;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Der enum Size beschreibt die moegliche Geschwindigkeit eines Items.
 *<br />
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public enum Speed {
    SLOW, MEDIUM, FAST
}//enum
