package a4;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Set;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //##########################################################################
    //######
    //######    Collector A
    //######
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet => 0 Punkte). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview_CA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "CollectorA";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( collector, "collector (A)" );
                //
                if( TS.isActualMethod( collector.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  collector.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  collector.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "CollectorA". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "CollectorA". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                      TS.isClass(             classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",       TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",       TS.isImplementing(      classUnderTest, "Collector_I" ));
            assertTrue( "requested constructor missing",     TS.isConstructor(       classUnderTest, null ));
            assertTrue( "false constructor access modifier", TS.isAccessModifierSet( classUnderTest, null, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorA" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "process", Collection.class, new Class<?>[]{ Item.class } ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "process", Collection.class, new Class<?>[]{ Item.class }, Modifier.PUBLIC ));     // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorA" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorA" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorA" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: CollectorA erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_CollectorA(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "reset()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_reset_CA_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "process()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_process_CA_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.CYAN, Size.MEDIUM, Speed.MEDIUM, 2L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.process( testParameter );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfacher Test: (NUR-)Methoden-Aufruf "process()"&"reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_combined_CA_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.FAST, 13L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.process( testParameter );
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Methoden-Aufruf "process()"&"reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_combined_CA_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=5; --stillToDo>=0; ){
                final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.FAST, (long)( stillToDo ));
                collector.process( testParameter );
            }//for
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CA_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.MEDIUM, 23L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertNull( collector.process( testParameter ) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" - ("nur") eine Sammlung zusammenstellen - bleibt Ordnung erhalten? */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CA_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int indxPar = 0;
            int indxChk = 0;
            //
            for( int i=0; i<4; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            final Collection<Item> resultExa = collector.process( ia[indxPar++] );
            assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
            if( resultExa instanceof Set ){
                fail( "Ordnung bleibt NICHT erhalten" );
            }//if
            for( final Item itemExa : resultExa ){
                assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" - Sammlungen zusammenstellen - bleibt Ordnung erhalten? */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CA_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int startIndx = 0;
            int indxPar = startIndx;
            int indxChk = startIndx;
            //
            do{
                for( int i=0; i<4 && indxPar<ia.length; i++ ){
                    assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                }//for
                //
                if( indxPar<ia.length ){
                    final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                    assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                    if( resultExa instanceof Set ){
                        fail( "Ordnung bleibt NICHT erhalten" );
                    }//if
                    for( final Item itemExa : resultExa ){
                        assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
                    }//for
                }//if
                //
                startIndx+=5;
            }while( indxPar<ia.length );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "1* reset()"   ( und auch process() ) */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reset_CA_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int indxPar = 0;
            int indxChk = 0;
            //
            for( int i=0; i<4; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            collector.reset();
            indxPar = 0;
            indxChk = 0;
            //
            for( int i=0; i<4; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            final Collection<Item> resultExa = collector.process( ia[indxPar++] );
            assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
            if( resultExa instanceof Set ){
                fail( "Ordnung bleibt NICHT erhalten" );
            }//if
            for( final Item itemExa : resultExa ){
                assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()  
    
    /** Funktions-Test: "n* reset()"   ( und auch process() ) */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reset_CA_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final int numberOfRuns=13;
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int startIndx = 0;
            int indxPar = startIndx;
            int indxChk = startIndx;
            //
            do{
                for( int i=0; i<4 && indxPar<ia.length; i++ ){
                    assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                }//for
                //
                for( int runNo=numberOfRuns; --runNo>=0; ){
                    collector.reset();
                    indxPar = startIndx;
                    indxChk = startIndx;
                    //
                    for( int i=0; i<4 && indxPar<ia.length; i++ ){
                        assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                    }//for
                }//for
                //
                if( indxPar<ia.length ){
                    final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                    assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                    if( resultExa instanceof Set ){
                        fail( "Ordnung bleibt NICHT erhalten" );
                    }//if
                    for( final Item itemExa : resultExa ){
                        assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
                    }//for
                }//if
                //
                startIndx+=5;
            }while( indxPar<ia.length );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Funktions-Test: "n* reset()"   ( und auch process() ) */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_combined_CA_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final int numberOfExtRuns=3;
            final int numberOfIntRuns=5;
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                 // #25 - NO doubles - 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,   13L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.MEDIUM, 97L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,    5L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,  2L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,   67L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 47L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   83L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,    7L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   37L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM, 31L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 19L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,   61L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   79L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   89L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   73L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 17L ),
                new Item( Color.RED,     Size.MEDIUM, Speed.FAST,   53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM, 23L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,    3L )
            };
            //
            for( int extRunNo=numberOfExtRuns; --extRunNo>=0; ){
                int startIndx=0;
                int indxPar;
                int indxChk;
                do{ 
                    collector.reset();
                    indxPar = startIndx;
                    indxChk = startIndx;
                    //
                    for( int i=0; i<4 && indxPar<ia.length; i++ ){
                        assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                    }//for
                    //
                    for( int runNo=numberOfIntRuns; --runNo>=0; ){
                        collector.reset();
                        indxPar = startIndx;
                        indxChk = startIndx;
                        //
                        for( int i=0; i<4 && indxPar<ia.length; i++ ){
                            assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                        }//for
                    }//for
                    //
                    if( indxPar<ia.length ){
                        final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                        assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                        if( resultExa instanceof Set ){
                            fail( "Ordnung bleibt NICHT erhalten" );
                        }//if
                        for( final Item itemExa : resultExa ){
                            assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
                        }//for
                    }//if
                    startIndx++;
                }while( indxPar<ia.length );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method() 
    
    /** Funktions-Test: "reset()" (und indirekt process() ) */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_combined_CA_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorA";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final int numberOfExtRuns=3;
            final int numberOfIntRuns=5;
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                 // with doubles
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,   13L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.MEDIUM, 97L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,    5L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,  2L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,   67L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 47L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   83L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,    7L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   37L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM, 31L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 19L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,   61L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   79L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   89L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   73L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 17L ),
                new Item( Color.RED,     Size.MEDIUM, Speed.FAST,   53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM, 23L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,    3L ),
                new Item( Color.PINK,    Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L )
            };
            //
            for( int extRunNo=numberOfExtRuns; --extRunNo>=0; ){
                int startIndx=0;
                int indxPar;
                int indxChk;
                do{ 
                    collector.reset();
                    indxPar = startIndx;
                    indxChk = startIndx;
                    //
                    for( int i=0; i<4 && indxPar<ia.length; i++ ){
                        assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                    }//for
                    //
                    for( int runNo=numberOfIntRuns; --runNo>=0; ){
                        collector.reset();
                        indxPar = startIndx;
                        indxChk = startIndx;
                        //
                        for( int i=0; i<4 && indxPar<ia.length; i++ ){
                            assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                        }//for
                    }//for
                    //
                    if( indxPar<ia.length ){
                        final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                        assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                        if( resultExa instanceof Set ){
                            fail( "Ordnung bleibt NICHT erhalten" );
                        }//if
                        for( final Item itemExa : resultExa ){
                            assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
                        }//for
                    }//if
                    startIndx++;
                }while( indxPar<ia.length );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //##########################################################################
    //######
    //######    Collector B
    //######
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle. */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview_CB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "CollectorB";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( collector, "collector (B)" );
                //
                if( TS.isActualMethod( collector.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  collector.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  collector.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "CollectorB". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "CollectorB". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                      TS.isClass(             classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",       TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",       TS.isImplementing(      classUnderTest, "Collector_I" ));
            assertTrue( "requested constructor missing",     TS.isConstructor(       classUnderTest, null ));
            assertTrue( "false constructor access modifier", TS.isAccessModifierSet( classUnderTest, null, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorB" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "process", Collection.class, new Class<?>[]{ Item.class } ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "process", Collection.class, new Class<?>[]{ Item.class }, Modifier.PUBLIC ));     // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorB" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorB" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorB" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: CollectorB erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_CollectorB(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "reset()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_reset_CB_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "process()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_process_CB_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.CYAN, Size.MEDIUM, Speed.MEDIUM, 2L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.process( testParameter );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B
    //###
    
    /** Einfacher Test: (NUR-)Methoden-Aufruf "process()"&"reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_combined_CB_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.FAST, 13L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.process( testParameter );
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Methoden-Aufruf "process()"&"reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_combined_CB_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=7; --stillToDo>=0; ){
                final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.FAST, (long)( stillToDo ));
                collector.process( testParameter );
            }//for
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CB_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.MEDIUM, 23L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertNull( collector.process( testParameter ) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CB_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int indxPar = 0;
            //
            for( int i=0; i<6; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            final Collection<Item> resultExa = collector.process( ia[indxPar++] );
            assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
            if( resultExa instanceof Set ){
                fail( "Ordnung kann NICHT richtig sein" );
            }//if
            int indxChk = indxPar;
            for( final Item itemExa : resultExa ){
                assertEquals( "Element falsch - u.U. Ordnung falsch?",  ia[--indxChk], itemExa );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CB_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int startIndx = 0;
            int indxPar = startIndx;
            int indxChk = startIndx;
            //
            do{
                for( int i=0; i<6 && indxPar<ia.length; i++ ){
                    assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                }//for
                //
                if( indxPar<ia.length ){
                    final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                    assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                    if( resultExa instanceof Set ){
                        fail( "Ordnung kann NICHT richtig sein" );
                    }//if
                    indxChk = indxPar;
                    for( final Item itemExa : resultExa ){
                        assertEquals( "Element falsch - u.U. Ordnung falsch?",  ia[--indxChk], itemExa );
                    }//for
                }//if
                //
                startIndx+=5;
            }while( indxPar<ia.length );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "reset()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reset_CB_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int indxPar = 0;
            int indxChk = 0;
            //
            for( int i=0; i<6; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            collector.reset();
            indxPar = 0;
            indxChk = 0;
            //
            for( int i=0; i<6; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            final Collection<Item> resultExa = collector.process( ia[indxPar++] );
            assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
            if( resultExa instanceof Set ){
                fail( "Ordnung kann NICHT richtig sein" );
            }//if
            indxChk = indxPar;
            for( final Item itemExa : resultExa ){
                assertEquals( "Element falsch - u.U. Ordnung falsch?",  ia[--indxChk], itemExa );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "reset()" (und indirekt process() ) */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reset_CB_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final int numberOfRuns=13;
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                                     // each color once
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,                         2L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,                           0L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_783L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM,                         1L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,                          41L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM,                        71L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,                        31L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,                          61L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM,                        11L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,   9_223_372_036_854_775_807L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,                           3L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,                          13L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,                          83L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,                          73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,                          53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,                        23L )
            };
            int startIndx = 0;
            int indxPar = startIndx;
            int indxChk = startIndx;
            //
            do{
                for( int i=0; i<6 && indxPar<ia.length; i++ ){
                    assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                }//for
                //
                for( int runNo=numberOfRuns; --runNo>=0; ){
                    collector.reset();
                    indxPar = startIndx;
                    indxChk = startIndx;
                    //
                    for( int i=0; i<6 && indxPar<ia.length; i++ ){
                        assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                    }//for
                }//for
                //
                if( indxPar<ia.length ){
                    final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                    assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                    if( resultExa instanceof Set ){
                        fail( "Ordnung kann NICHT richtig sein" );
                    }//if
                    indxChk = indxPar;
                    for( final Item itemExa : resultExa ){
                        assertEquals( "Element falsch - u.U. Ordnung falsch?",  ia[--indxChk], itemExa );
                    }//for
                }//if
                //
                startIndx+=5;
            }while( indxPar<ia.length );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Funktions-Test: "reset()" (und indirekt process() ) */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_combined_CB_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final int numberOfExtRuns=3;
            final int numberOfIntRuns=5;
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                 // #25 - NO doubles - 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,   13L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.MEDIUM, 97L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,    5L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,  2L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,   67L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 47L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   83L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,    7L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   37L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM, 31L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 19L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,   61L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   79L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   89L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   73L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 17L ),
                new Item( Color.RED,     Size.MEDIUM, Speed.FAST,   53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM, 23L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,    3L )
            };
            //
            for( int extRunNo=numberOfExtRuns; --extRunNo>=0; ){
                int startIndx=0;
                int indxPar;
                int indxChk;
                do{ 
                    collector.reset();
                    indxPar = startIndx;
                    indxChk = startIndx;
                    //
                    for( int i=0; i<6 && indxPar<ia.length; i++ ){
                        assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                    }//for
                    //
                    for( int runNo=numberOfIntRuns; --runNo>=0; ){
                        collector.reset();
                        indxPar = startIndx;
                        indxChk = startIndx;
                        //
                        for( int i=0; i<6 && indxPar<ia.length; i++ ){
                            assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                        }//for
                    }//for
                    //
                    if( indxPar<ia.length ){
                        final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                        assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                        if( resultExa instanceof Set ){
                            fail( "Ordnung kann NICHT richtig sein" );
                        }//if
                        indxChk = indxPar;
                        for( final Item itemExa : resultExa ){
                            assertEquals( "Element falsch - u.U. Ordnung falsch?",  ia[--indxChk], itemExa );
                        }//for
                    }//if
                    startIndx++;
                }while( indxPar<ia.length );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method() 
    
    /** Funktions-Test: "reset()" (und indirekt process() ) */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_combined_CB_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorB";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final int numberOfExtRuns=3;
            final int numberOfIntRuns=5;
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {                                                 // with doubles
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,   13L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.MEDIUM, 97L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,    5L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,  2L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,   67L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 47L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   83L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,    7L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   37L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM, 31L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 19L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,   61L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   79L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   89L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   73L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 17L ),
                new Item( Color.RED,     Size.MEDIUM, Speed.FAST,   53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM, 23L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,    3L ),
                new Item( Color.PINK,    Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L )
            };
            //
            for( int extRunNo=numberOfExtRuns; --extRunNo>=0; ){
                int startIndx=0;
                int indxPar;
                int indxChk;
                do{ 
                    collector.reset();
                    indxPar = startIndx;
                    indxChk = startIndx;
                    //
                    for( int i=0; i<6 && indxPar<ia.length; i++ ){
                        assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                    }//for
                    //
                    for( int runNo=numberOfIntRuns; --runNo>=0; ){
                        collector.reset();
                        indxPar = startIndx;
                        indxChk = startIndx;
                        //
                        for( int i=0; i<6 && indxPar<ia.length; i++ ){
                            assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
                        }//for
                    }//for
                    //
                    if( indxPar<ia.length ){
                        final Collection<Item> resultExa = collector.process( ia[indxPar++] );
                        assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
                        if( resultExa instanceof Set ){
                            fail( "Ordnung kann NICHT richtig sein" );
                        }//if
                        indxChk = indxPar;
                        for( final Item itemExa : resultExa ){
                            assertEquals( "Element falsch - u.U. Ordnung falsch?",  ia[--indxChk], itemExa );
                        }//for
                    }//if
                    startIndx++;
                }while( indxPar<ia.length );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //##########################################################################
    //######
    //######    Collector C
    //######
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle. */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview_CC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "CollectorC";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( collector, "collector (C)" );
                //
                if( TS.isActualMethod( collector.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  collector.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  collector.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "CollectorC". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "CollectorC". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                      TS.isClass(             classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",       TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",       TS.isImplementing(      classUnderTest, "Collector_I" ));
            assertTrue( "requested constructor missing",     TS.isConstructor(       classUnderTest, null ));
            assertTrue( "false constructor access modifier", TS.isAccessModifierSet( classUnderTest, null, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorC" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "process", Collection.class, new Class<?>[]{ Item.class } ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "process", Collection.class, new Class<?>[]{ Item.class }, Modifier.PUBLIC ));     // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorC" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorC" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CollectorC" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: CollectorC erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_CollectorC(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "reset()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_reset_CC_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "process()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_process_CC_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.CYAN, Size.MEDIUM, Speed.MEDIUM, 2L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.process( testParameter );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfacher Test: (NUR-)Methoden-Aufruf "process()"&"reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_combined_CC_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.FAST, 13L );
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            collector.process( testParameter );
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Methoden-Aufruf "process()"&"reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_combined_CC_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=4; --stillToDo>=0; ){
                final Item testParameter = new Item( Color.GREEN, Size.SMALL, Speed.FAST, (long)( stillToDo ));
                collector.process( testParameter );
            }//for
            collector.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Funktions-Test: "process()" fuer 3 Items. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CC_3items_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,    73L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.FAST,    53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM,  23L )
            };
            for( int i=0; i<3; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[i] ) );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CC_4itemsOfDifferentColor_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {
                new Item( Color.BLACK, Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,  Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN, Size.SMALL,  Speed.FAST,   37L ),
                new Item( Color.TAN,   Size.LARGE,  Speed.MEDIUM, 31L )
            };
            for( int i=0; i<4; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[i] ) );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CC_4itemsOfDifferentColor_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {
                new Item( Color.BLACK, Size.MEDIUM, Speed.MEDIUM, 13L ),
                new Item( Color.CYAN,  Size.MEDIUM, Speed.MEDIUM, 13L ),
                new Item( Color.BROWN, Size.MEDIUM, Speed.MEDIUM, 13L ),
                new Item( Color.TAN,   Size.MEDIUM, Speed.MEDIUM, 13L )
            };
            for( int i=0; i<4; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[i] ) );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CC_no3_4itemsOfSameColor(){                // __???__<170119> TODO : NAME
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {
                new Item( Color.BLACK, Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.BLACK, Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BLACK, Size.SMALL,  Speed.FAST,   37L ),
                new Item( Color.BLACK, Size.LARGE,  Speed.MEDIUM, 31L )
            };
            int indxPar = 0;
            int indxChk = 0;
            //
            for( int i=0; i<3; i++ ){
                assertNull( "nothing/null expected", collector.process( ia[indxPar++] ));
            }//for
            //
            final Collection<Item> resultExa = collector.process( ia[indxPar++] );
            assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null", resultExa );
            if( resultExa instanceof Set ){
                fail( "Ordnung bleibt NICHT erhalten" );
            }//if
            for( final Item itemExa : resultExa ){
                assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[indxChk++], itemExa );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "process()" */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_process_CC_no1(){                                  // __???__<170119> TODO : NAME
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,   13L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.MEDIUM, 97L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,    5L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,  2L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,   67L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 47L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   83L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,    7L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   37L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM, 31L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 19L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,   61L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   79L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   89L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   73L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 17L ),
                new Item( Color.RED,     Size.MEDIUM, Speed.FAST,   53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM, 23L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,    3L ),
                new Item( Color.PINK,    Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   31L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.SLOW,   43L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L )
            };
            final int[][] expectedDectection = {{1, 7, 13, 15}, {10, 17, 21, 32}, {22, 30, 35, 36}, {24, 33, 37, 39}, {ia.length}};
            //
            //
            int indxPar = 0;
            int indxHit = 0;
            do{
                final int nextHit = expectedDectection[indxHit][expectedDectection[indxHit].length-1];
                while( indxPar<nextHit && indxPar<ia.length ){
                    assertNull( "nothing/null expected",  collector.process( ia[indxPar] ));
                    indxPar++;
                }//while
                //
                if( indxPar<ia.length ){
                    final Collection<Item> resultExa = collector.process( ia[indxPar] );
                    assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null",  resultExa );
                    if( resultExa instanceof Set ){
                        fail( "Ordnung bleibt NICHT erhalten" );
                    }//if
                    int indxExp=0;
                    for( final Item itemExa : resultExa ){
                        final int theIndxHit = expectedDectection[indxHit][indxExp];
                        assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[theIndxHit], itemExa );
                        indxExp++;
                    }//for
                    indxPar++;
                    indxHit++;
                }//if
            }while( indxPar<ia.length );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Funktions-Test: combined */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_combined_CC_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CollectorC";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Collector_I collector = (Collector_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Item[] ia = {
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   43L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM,  1L ),
                new Item( Color.TAN,     Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.SLOW,   13L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.FAST,   71L ),
                new Item( Color.ORANGE,  Size.SMALL,  Speed.FAST,   13L ),
                new Item( Color.PINK,    Size.MEDIUM, Speed.MEDIUM, 97L ),
                new Item( Color.MAGENTA, Size.LARGE,  Speed.FAST,    5L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.SLOW,   29L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM,  2L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.SLOW,   67L ),
                //
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 47L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.SLOW,   83L ),
                new Item( Color.BLUE,    Size.LARGE,  Speed.SLOW,   71L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,    7L ),
                new Item( Color.BLACK,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   37L ),
                new Item( Color.TAN,     Size.LARGE,  Speed.MEDIUM, 31L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 19L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.FAST,   37L ),
                new Item( Color.GRAY,    Size.MEDIUM, Speed.SLOW,   61L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   79L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.FAST,   29L ),
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   89L ),
                new Item( Color.PINK,    Size.SMALL,  Speed.MEDIUM, 97L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.SLOW,   43L ),
                //
                new Item( Color.PURPLE,  Size.MEDIUM, Speed.FAST,   73L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.BROWN,   Size.MEDIUM, Speed.MEDIUM, 17L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.FAST,   29L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   73L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.VIOLET,  Size.SMALL,  Speed.MEDIUM, 59L ),
                new Item( Color.CYAN,    Size.MEDIUM, Speed.MEDIUM, 71L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.SLOW,   71L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   41L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.GOLD,    Size.SMALL,  Speed.SLOW,   29L ),
                new Item( Color.YELLOW,  Size.LARGE,  Speed.FAST,   67L ),
                new Item( Color.BROWN,   Size.SMALL,  Speed.SLOW,   19L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   61L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.FAST,   53L ),
                new Item( Color.BROWN,   Size.LARGE,  Speed.FAST,   31L ),
                new Item( Color.WHITE,   Size.SMALL,  Speed.MEDIUM, 11L ),
                new Item( Color.RED,     Size.MEDIUM, Speed.FAST,   53L ),
                new Item( Color.GREEN,   Size.SMALL,  Speed.MEDIUM, 23L ),
                new Item( Color.RED,     Size.LARGE,  Speed.FAST,    3L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.FAST,   11L ),
                new Item( Color.GREEN,   Size.LARGE,  Speed.FAST,   23L ),
                new Item( Color.BLUE,    Size.SMALL,  Speed.SLOW,   23L )
            };
            final int[] resetIndexVector = { 13, 32, Integer.MAX_VALUE };
            final int[][] expectedDectection = {{1, 2, 5, 9}, {15, 23, 27, 31}, {35, 36, 41, 42}, {34, 40, 46, 49}, {43, 47, 48, 52}, {ia.length}};
            //
            //
            int indxPar = 0;
            int indxHit = 0;
            int resetIndex = 0;
            do{
                final int nextHit = expectedDectection[indxHit][expectedDectection[indxHit].length-1];
                while( indxPar<nextHit && indxPar<ia.length ){
                    if( resetIndexVector[resetIndex] == indxPar ){
                        collector.reset();
                        resetIndex++;
                    }//if
                    //
                    Collection<Item> coll = collector.process( ia[indxPar] );
                    if( null != coll ){
                        Herald.proclaimMessage( SYS_OUT,  String.format(
                            "@%s():\n" 
                          + "nothing/null expected,  but found:\n"
                          + "%s ;  number-of-entries:%d ;  @internal-index:%d\n",
                            testName,
                            coll,
                            coll.size(),
                            indxPar
                        ));
                    }
                    assertNull( "nothing/null expected", coll );
                    indxPar++;
                }//while
                //
                if( indxPar<ia.length ){
                    final Collection<Item> resultExa = collector.process( ia[indxPar] );
                    assertNotNull( "(Echtes) Ergebnis erwartet und NICHT null",  resultExa );
                    if( resultExa instanceof Set ){
                        fail( "Ordnung bleibt NICHT erhalten" );
                    }//if
                    int indxExp=0;
                    for( final Item itemExa : resultExa ){
                        final int theIndxHit = expectedDectection[indxHit][indxExp];
                        assertEquals( "Element falsch - Ordnung nicht erhalten?",  ia[theIndxHit], itemExa );
                        indxExp++;
                    }//for
                    indxPar++;
                    indxHit++;
                }//if
            }while( indxPar<ia.length );           
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 56;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A4;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
