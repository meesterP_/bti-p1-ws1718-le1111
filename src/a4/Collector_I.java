package a4;


import java.util.Collection;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Das Interface Collector_I
 * <ul>
 *     <li>beschreibt einen Collector, der Datensaetze vom Typ Item aufnehmen kann und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und
 *         fordert die entsprechenden Methoden ein.
 *     </li>
 * </ul>
 * Die von Ihnen zu implementierenden Klassen muessen einen oeffentlichen parameterlosen Konstruktor aufweisen.
 *<br />
 *<br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse
 * findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public interface Collector_I {
    
    /**
     * Diese Operation verarbeitet ein Item.<br />
     * Der von Ihnen zu implementierende Collector reagiert auf das jeweils (neu) erscheinende Item.
     * Die jeweilige Reaktion wird von der Operation process realisiert.
     * Das jeweils neu erscheinende Item wird konkret als Parameter an die Operation process uebergeben
     * und muss geeignet intern verabeitet werden.<br />
     * <ul>
     *     <li>Im Falle von CollectorA:<br />
     *         Immer sobald fuenf Items vorliegen, sollen diese die Eintreff-Reihenfolge erhaltend
     *         als Rueckgabewert der Operation (in Form einer geeigneten Collection) abgeliefert
     *         und aus einem moeglichen internen "Gedaechtnis" entfernt werden,
     *         <br />
     *         andernfalls soll null zurückgegeben werden.
     *     </li>
     *     <li>Im Falle von CollectorB:<br />
     *         Immer sobald sieben Items vorliegen, sollen diese in umgekehrter Reihenfolge
     *         <i>bzw. umgekehrt zur Eintreff-Reihenfolge</i>
     *         (in Form einer geeigneten Collection) abgeliefert
     *         und aus einem moeglichen internen "Gedaechtnis" entfernt werden,
     *         <br />
     *         andernfalls soll null zurückgegeben werden.
     *     </li>
     *     <li>Im Falle von CollectorC:<br />
     *         Sobald vier Items gleicher Farbe vorliegen, sollen diese die Eintreff-Reihenfolge
     *         erhaltend als Rueckgabewert der Operation (in Form einer geeigneten Collection)
     *         abgeliefert und aus einem moeglichen internen "Gedaechtnis" entfernt werden,
     *         <br />
     *         andernfalls soll null zurückgegeben werden.
     *     </li>
     * </ul>
     *<br />
     * Bemerkungen:
     * <ul>
     *     <li>Ein Item, das bereits Teil einer abgelieferten (Ergebnis-)Collection war,
     *         darf nicht fuer die Bildung einer anderen (Ergebnis-)Collection verwendet werden.
     *         Diese Aussage betrifft die Identitaet - "Gleiche" sind unbetroffen.
     *         <br />
     *         (<i>Moegliche interne Collections sind hiervon ausgenommen.</i>)
     *     </li>
     *     <li>Dasselbe Item erscheint nur einmal. Gleiche Items koennen mehrfach erschein.
     *     </li>
     *
     *
     * @param item bestimmt das (neue) Item, das zu verarbeiten ist.
     * @return  der jeweils geforderte Collection unmittelbar nachdem sie gebildet werden kann<br />
     *          und sonst null.
     */
    Collection<Item> process( Item item );
    
    /**
     * Diese Operation setzt einen moeglichen (internen) Zustand auf den Ausgangswert bzw. die Starteinstellung zurueck.<br />
     * (<i>reset macht halt einen reset. Der Name sollte selbsterklaerend sein.</i>)
     */
    void reset();
    
}//interface
