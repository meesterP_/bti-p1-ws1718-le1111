package a4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CollectorA implements Collector_I {
    private List<Item> list;

    public CollectorA() {
        list = new ArrayList<Item>();        
    }

    @Override
    public Collection<Item> process(Item item) {
        list.add(item);
        if (list.size() >= 5) {
            List<Item> listToReturn = list;
            reset();
            return listToReturn;
        } else {
            return null;
        }
    }

    @Override
    public void reset() {
        list.clear();
    }

}
