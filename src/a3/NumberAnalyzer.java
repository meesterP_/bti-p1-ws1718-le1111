package a3;

import java.util.ArrayList;
import java.util.List;

public class NumberAnalyzer implements NumberAnalyzer_I {
    private long number = 0L;

    public NumberAnalyzer(long number) {
        assert number > -1 : "illegal paramter!";

        this.number = number;
    }

    @Override
    public long getNumber() {
        return number;
    }

    @Override
    public byte[] toDigitArray() {
        List<Byte> list = new ArrayList<Byte>();
        long digit = 10;
        long workNumber = number;
        if (workNumber != 0) {
            while (workNumber > 0) {
                long modResult = workNumber % digit;
                list.add((byte) modResult);
                workNumber /= digit;
            }
        } else {
            list.add((byte) workNumber);
        }

        byte[] arrayToReturn = new byte[list.size()];
        for (int i = 0; i < list.size(); i++) {
            arrayToReturn[i] = list.get(i);
        }
        return arrayToReturn;
    }

    @Override
    public boolean isPalindromicNumber() {
        String numberAsString = Long.toString(number);
        int firstPos = 0;
        int lastPos = numberAsString.length() - 1;
        char firstChar = numberAsString.charAt(firstPos);
        char lastChar = numberAsString.charAt(lastPos);

        while (firstChar == lastChar) {
            if (firstPos >= lastPos) {
                return true;
            }
            firstPos++;
            lastPos--;
            firstChar = numberAsString.charAt(firstPos);
            lastChar = numberAsString.charAt(lastPos);
        }
        return false;
    }

}
