package a3;

public class TestFrame {

    public static void main(String[] args) {
        NumberAnalyzer na = new NumberAnalyzer(9L);
        byte[] array = na.toDigitArray();
        for (byte e : array) {
            System.out.println(e);
        }
    }

}
