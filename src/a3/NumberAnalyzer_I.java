package a3;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Das Interface NumberAnalyzer_I
 * <ul>
 *     <li>beschreibt einen NumberAnalyzer und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und
 *         fordert die entsprechenden Methoden ein.
 *     </li>
 * </ul>
 * Ein NumberAnalyzer soll nichtnegative ganzen Zahlen untersuchen und
 * insbesondere auch ueberpruefen koennen, ob eine Palindromzahl vorliegt.
 *<br />
 * Zu beachten ist, dass
 * <ul>
 *     <li>in dieser Aufgabe bei allen Betrachtungen
 *         das Dezimalsystem zugrunde gelegt wird!
 *     </li>
 *     <li>(<i>fuer als Parameter an den Konstruktor uebergebene Zahlen</i>)
 *         fuehrende Nullen nicht relevant sind!<br />
 *         Die Zahl 0 besteht einzig aus genau einer Ziffer 0.
 *     </li>
 *     <li>nur NumberAnalyzer-Objekte fuer zulaessige Zahlen
 *         (also nicht-negative ganze Zahlen) erzeugt werden!
 *         <br />
 *         Nicht akzeptierbare Zahlen sind auf die in der Vorlesung erlernte Art (Stichwort: assert) zurueckzuweisen.<br />
 *         Optional darf auch eine geeignete(!) Exception als Alternative fuer die Zurueckweisung verwendet werden.
 *         <br />
 *         <i>Bemerkung: Code fuer Akzeptanztests, ob ein als Parameter uebergebenes Array geeignet ist, zaehlen zum "C-Level". </i><br />
 *     </li>
 * </ul>
 *<br />
 * Die von Ihnen zu implementierende Klasse NumberAnalyzer muss
 * <ul>
 *     <li>einen oeffentlichen Konstruktor aufweisen,
 *         der der folgenden Signatur genuegt:<br />
 *         <code>NumberAnalyzer( long )</code>
 *     </li>
 * </ul>
 *<br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse
 * findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public interface NumberAnalyzer_I {
    
    /**
     * getNumber() liefert die zuvor dem Konstruktor uebergebene Zahl.
     *
     * 
     * @return  Zahl
     */
    long getNumber();
    
    /**
     * toDigitArray() liefert ein Array ueber die einzelnen (Dezimal-)Ziffern
     * der (zuvor dem Konstruktor uebergebenen) Zahl.
     * Die Ziffer an Stelle n bzw. 10^n soll im Array an der Position n abgelegt werden.
     * <br />
     *<br />
     * Also zum Beispiel:<br />
     * Die Ziffer an der Stelle 10^0 ("Einer") soll im Array an der Position 0 abgelegt werden.<br />
     * Die Ziffer an der Stelle 10^1 ("Zehner") soll im Array an der Position 1 abgelegt werden (<i>sofern ein "Zehner" vorhanden bzw. die Zahl groesser 9 ist</i>).<br />
     * Die Ziffer an der Stelle 10^2 ("Hunderter") soll im Array an der Position 2 abgelegt werden (<i>sofern ein "Hunderter" vorhanden bzw. die Zahl groesser 99 ist</i>).<br />
     * Die Ziffer an der Stelle 10^3 ("Tausender") soll im Array an der Position 3 abgelegt werden (<i>sofern ein "Tausender" vorhanden bzw. die Zahl groesser 999 ist</i>).<br />
     * Und so weiter.
     *
     * 
     * @return  Array ueber die einzelnen Ziffern der jeweiligen Zahl
     */
    byte[] toDigitArray();
    
    /**
     * Achtung! Wir erinnern uns, dass in dieser Aufgabe bei allen Betrachtungen
     * das Dezimalsystem zugrunde gelegt wird!<br />
     * Die Methode isPalindromicNumber() ueberprueft die (zuvor dem Konstruktor uebergebene)
     * Zahl darauf, ob sie eine Palindromzahl ist.
     * Palindromzahlen (bzw. Zahlenpalindrome) sind Zahlen,
     * die von vorne und hinten gelesen den gleichen Wert haben, z. B. 93139 oder 89422498.
     * 
     * 
     * @return  <code>true</code>, wenn eine Palindromzahl vorliegt und
     *          sonst <code>false</code>.
     */
    boolean isPalindromicNumber();
    
}//interface
