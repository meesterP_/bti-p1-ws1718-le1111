package a3;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A / "1e"
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet => 0 Punkte). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "NumberAnalyzer";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final long testNumber = 121L;   // anything ;-)
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final NumberAnalyzer_I numberAnalyzer = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
                TS.printDetailedInfoAboutObject( numberAnalyzer, "numberAnalyzer" );
                //
                if( TS.isActualMethod( numberAnalyzer.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  numberAnalyzer.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  numberAnalyzer.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "NumberAnalyzer". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "NumberAnalyzer". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(             classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "NumberAnalyzer_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, new Class<?>[]{ long.class } ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, new Class<?>[]{ long.class }, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "NumberAnalyzer" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "isPalindromicNumber", boolean.class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "toDigitArray",        byte[].class,  null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "getNumber",           long.class,    null ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "isPalindromicNumber", boolean.class, null, Modifier.PUBLIC ));     // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "toDigitArray",        byte[].class,  null, Modifier.PUBLIC ));     // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "getNumber",           long.class,    null, Modifier.PUBLIC ));     // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "NumberAnalyzer" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "NumberAnalyzer" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "NumberAnalyzer" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: NumberAnalyzer erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation__NumberAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 121L;   // anything ;-)
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getNumber()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getNumber_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 121L;   // anything ;-)
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            na.getNumber();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_toDigitArray_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 121L;   // anything ;-)
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            na.toDigitArray();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "isPalindromicNumber()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_isPalindromicNumber_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 121L;   // anything ;-)
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            na.isPalindromicNumber();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()      
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Funktions-Test: "getNumber()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getNumber_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 1L;
        final long expectedResult = testNumber;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertEquals( expectedResult, na.getNumber() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_toDigitArray_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 1L;
        final byte[] expectedResult = { 1 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isPalindromicNumberWith3DigitsPositive(){
        subTestBehavior_isPalindromicNumber( 888L,  true,  1,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isPalindromicNumberWith3DigitsNegative(){
        subTestBehavior_isPalindromicNumber( 345L,  false,  1,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isPalindromicNumberWith4DigitsPositive(){
        subTestBehavior_isPalindromicNumber( 8888L,  true,  1,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isPalindromicNumberWith4DigitsNegative(){
        subTestBehavior_isPalindromicNumber( 6789L,  false,  1,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "getNumber()"
    //
    
    /** Einfacher Test: Funktion "getNumber()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumber_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 13L;
        final long expectedResult = testNumber;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertEquals( expectedResult, na.getNumber() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumber()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumber_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 42L;
        final long expectedResult = testNumber;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertEquals( expectedResult, na.getNumber() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
        
    /** Funktions-Test: "getNumber()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumber_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 0L;
        final long expectedResult = testNumber;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertEquals( expectedResult, na.getNumber() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    /** Funktions-Test: "getNumber()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumber_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = Long.MAX_VALUE;
        final long expectedResult = testNumber;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertEquals( expectedResult, na.getNumber() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "toDigitArray()"
    //
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 121L;
        final byte[] expectedResult = { 1, 2, 1 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 8008L;
        final byte[] expectedResult = { 8, 0, 0, 8 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 123L;
        final byte[] expectedResult = { 3, 2, 1 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 2345L;
        final byte[] expectedResult = { 5, 4, 3, 2 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 34567L;
        final byte[] expectedResult = { 7, 6, 5, 4, 3 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no6(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 456789L;
        final byte[] expectedResult = { 9, 8, 7, 6, 5, 4 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no7(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 5678901L;
        final byte[] expectedResult = { 1, 0, 9, 8, 7, 6, 5 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no8(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 67890123L;
        final byte[] expectedResult = { 3, 2, 1, 0, 9, 8, 7, 6 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no9(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 789012345L;
        final byte[] expectedResult = { 5, 4, 3, 2, 1, 0, 9, 8, 7 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no10(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 8901234567L;
        final byte[] expectedResult = { 7, 6, 5, 4, 3, 2, 1, 0, 9, 8 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no11(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 90123456789L;
        final byte[] expectedResult = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no12(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 123456789012L;
        final byte[] expectedResult = { 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no13(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 2345678901234L;
        final byte[] expectedResult = { 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no14(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 34567890123456L;
        final byte[] expectedResult = { 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no15(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 456789012345678L;
        final byte[] expectedResult = { 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no16(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 5678901234567890L;
        final byte[] expectedResult = { 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no17(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 67890123456789012L;
        final byte[] expectedResult = { 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no18(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 789012345678901234L;
        final byte[] expectedResult = { 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_toDigitArray_no19(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 8901234567890123456L;
        final byte[] expectedResult = { 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "isPalindromicNumber()"
    //
    
    /** Funktions-Test: "isPalindromicNumber()" 1 digit. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith1DigitPositive(){
        final long[] testParameter = { 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 2 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith2DigitsPositive(){
        final long[] testParameter = { 11L, 22L, 33L, 44L, 55L, 66L, 77L, 88L, 99L };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 2 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith2DigitsNegative(){
        final long[] testParameter = { 16L, 72L, 38L, 94L, 50L, 41L, 70L, 36L, 96L };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 3 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith3DigitsPositive(){
        final long[] testParameter = { 161L, 272L, 383L, 494L, 505L, 616L, 727L, 838L, 949L, 141L, 474L, 707L, 363L, 696L, 929L, 252L, 585L, 818L };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 3 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith3DigitsNegative(){
        final long[] testParameter = {
            123L, 456L, 789L,
            963L, 630L, 307L, 741L, 418L, 185L, 852L, 529L, 296L,
            166L, 772L, 388L, 994L, 500L,
            966L, 558L, 744L, 336L, 552L, 114L, 300L, 992L, 388L, 774L, 366L, 552L, 144L, 330L, 922L, 118L, 700L  
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 4 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith4DigitsPositive(){
        final long[] testParameter = {
            8008L, 1221L,
            1661L, 2772L, 3883L, 4994L, 5005L, 6116L, 7227L, 8338L, 9449L,
            1441L, 4774L, 7007L, 3663L, 6996L, 9229L, 2552L, 5885L, 8118L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 4 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith4DigitsNegative(){
        final long[] testParameter = {
            3888L, 8880L,
            1234L, 4567L, 7890L,
            9630L, 6307L, 3074L, 7418L, 4185L, 1852L, 8529L, 5296L, 2963L,
            1166L, 7722L, 3388L, 9944L, 5500L,
            9966L, 5588L, 7744L, 3366L, 5522L, 1144L, 3300L, 9922L, 3388L, 7744L, 3366L, 5522L, 1144L, 3300L, 9922L, 1188L, 7700L,
            6161L, 7272L, 3838L, 9494L, 5050L,
            9696L, 5858L, 7474L, 3636L, 5252L, 1414L, 3030L, 9292L, 3838L, 7474L, 3636L, 5252L, 1414L, 3030L, 9292L, 1818L, 7070L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 5 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith5DigitsPositive(){
        final long[] testParameter = {
            12321L,
            12321L, 45654L, 78987L,
            97479L, 85158L, 73837L, 61516L, 59295L, 46964L, 34643L,
            25252L, 37373L, 49494L, 51515L, 63636L, 75757L, 87878L,
            11111L, 22222L, 33333L, 44444L, 55555L, 66666L, 77777L, 88888L, 99999L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 5 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith5DigitsNegative(){
        final long[] testParameter = {
            38888L, 88880L,
            98765L,
            45566L, 77665L,
            78789L, 89089L, 90101L,
            99996L, 99969L, 96999L, 69999L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 6 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith6DigitsPositive(){
        final long[] testParameter = {
            123321L,
            777777L, 665566L, 433334L,
            246642L, 369963L, 482284L, 505505L, 631136L, 853358L, 975579L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 6 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith6DigitsNegative(){
        final long[] testParameter = {
            388888L, 888880L,
            888882L, 888828L, 888288L, 882888L, 828888L, 288888L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 7 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith7DigitsPositive(){
        final long[] testParameter = {
            1234321L,
            2468642L, 3692963L, 6493946L, 8516158L,
            4445444L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 7 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith7DigitsNegative(){
        final long[] testParameter = {
            3888888L, 8888880L,
            1234567L, 8642086L, 7418529L,
            5444444L, 4544444L, 4454444L, 4444544L, 4444454L, 4444445L,
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 8 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith8DigitsPositive(){
        final long[] testParameter = {
            12344321L,
            55555555L,
            24688642L, 36922963L, 64933946L, 85166158L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 8 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith8DigitsNegative(){
        final long[] testParameter = {
            38888888L, 88888880L,
            12345678L, 97531975L, 63074185L,
            27777777L, 72777777L, 77277777L, 77727777L, 77772777L, 77777277L, 77777727L, 77777772L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    
    /** Funktions-Test: "isPalindromicNumber()" 9 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith9DigitsPositive(){
        final long[] testParameter = {
            123454321L,
            246808642L, 369252963L, 469383964L, 851636158L,
            555525555L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 9 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith9DigitsNegative(){
        final long[] testParameter = {
            388888898L, 888988880L,
            123456789L, 975319753L, 630741852L,
            255555555L, 525555555L, 552555555L, 555255555L, 555552555L, 555555255L, 555555525L, 555555552L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 10 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith10DigitsPositive(){
        final long[] testParameter = {
            1234554321L,
            2468008642L, 3692552963L, 4693883964L, 8516336158L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 10 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith10DigitsNegative(){
        final long[] testParameter = {
            3888888898L, 8888988880L,
            1234567890L, 9753197531L, 6307418529L,
            3777777777L, 7377777777L, 7737777777L, 7773777777L, 7777377777L, 7777737777L, 7777773777L, 7777777377L, 7777777737L, 7777777773L
        };
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 11 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith11DigitsPositive(){
        final long[] testParameter = {
            12345654321L,
            46802420864L, 69258185296L, 12471617421L, 85160306158L
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 11 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith11DigitsNegative(){
        final int numberOfDigits = 11;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '1', '9' );
        testParameter[numberOfDigits+0] = 38888888888L;
        testParameter[numberOfDigits+1] = 88888888880L;
        testParameter[numberOfDigits+2] = 12345678901L;                         // "+1"
        testParameter[numberOfDigits+3] = 97531975319L;                         // "-2"
        testParameter[numberOfDigits+4] = 63074185296L;                         // "-3" 
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 12 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith12DigitsPositive(){
        final long[] testParameter = {
            123456654321L,                                                      // "+1"
            468024420864L,                                                      // "+2"
            692581185296L,                                                      // "+3"
            124716617421L,                                                      // "+1, +2, +3, ..."
            851603306158L                                                       // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 12 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith12DigitsNegative(){
        final int numberOfDigits = 12;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '2', '8' );
        testParameter[numberOfDigits+0] = 388888888888L;
        testParameter[numberOfDigits+1] = 888888888880L;
        testParameter[numberOfDigits+2] = 123456789012L;                        // "+1"
        testParameter[numberOfDigits+3] = 753197531975L;                        // "-2"
        testParameter[numberOfDigits+4] = 307418529630L;                        // "-3"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 13 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith13DigitsPositive(){
        final long[] testParameter = {
            1234567654321L,                                                     // "+1"
            6802468642086L,                                                     // "+2"
            9258136318529L,                                                     // "+3"
            1247162617421L,                                                     // "+1, +2, +3, ..."
            8516035306158L                                                      // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 13 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith13DigitsNegative(){
        final int numberOfDigits = 13;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 3888888889888L;
        testParameter[numberOfDigits+1] = 8888888888980L;
        testParameter[numberOfDigits+2] = 1234567890123L;                       // "+1"
        testParameter[numberOfDigits+3] = 5319753197531L;                       // "-2"
        testParameter[numberOfDigits+4] = 7418529630741L;                       // "-3"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 14 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith14DigitsPositive(){
        final long[] testParameter = {
            12345677654321L,                                                    // "+1"
            80246800864208L,                                                    // "+2"
            25813699631852L,                                                    // "+3"
            12471622617421L,                                                    // "+1, +2, +3, ..."
            85160355306158L                                                     // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 14 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith14DigitsNegative(){
        final int numberOfDigits = 14;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 38888888898888L;
        testParameter[numberOfDigits+1] = 88888888889880L;
        testParameter[numberOfDigits+2] = 12345678901234L;                      // "+1"
        testParameter[numberOfDigits+3] = 31975319753197L;                      // "-2"
        testParameter[numberOfDigits+4] = 41852963074185L;                      // "-3"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 15 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith15DigitsPositive(){
        final long[] testParameter = {
            123456787654321L,                                                   // "+1"
            468024686420864L,                                                   // "+2 & shift"
            581470363074185L,                                                   // "+3 & shift"
            124716292617421L,                                                   // "+1, +2, +3, ..."
            851603565306158L                                                    // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 15 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith15DigitsNegative(){
        final int numberOfDigits = 15;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 388888888988888L;
        testParameter[numberOfDigits+1] = 888888888898880L;
        testParameter[numberOfDigits+2] = 123456789012345L;                    // "+1"
        testParameter[numberOfDigits+3] = 197531975319753L;                    // "-2 & shift"
        testParameter[numberOfDigits+4] = 185296307418529L;                    // "-3 & shift"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 16 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith16DigitsPositive(){
        final long[] testParameter = {
            1234567887654321L,                                                  // "+1"
            6802468008642086L,                                                  // "+2 & shift"
            8147036996307418L,                                                  // "+3 & shift"
            1247162992617421L,                                                  // "+1, +2, +3, ..."
            8516035665306158L                                                   // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 16 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith16DigitsNegative(){
        final int numberOfDigits = 16;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 3888888888988888L;
        testParameter[numberOfDigits+1] = 8888888888898880L;
        testParameter[numberOfDigits+2] = 1234567890123456L;                    // "+1"
        testParameter[numberOfDigits+3] = 9753197531975319L;                    // "-2 & shift"
        testParameter[numberOfDigits+4] = 8529630741852963L;                    // "-3 & shift"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 17 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith17DigitsPositive(){
        final long[] testParameter = {
            12345678987654321L,                                                 // "+1"
            80246802420864208L,                                                 // "+2 & shift"
            14703692529630741L,                                                 // "+3 & shift"
            12471629792617421L,                                                 // "+1, +2, +3, ..."
            85160356565306158L                                                  // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 17 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith17DigitsNegative(){
        final int numberOfDigits = 17;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 38888888889888888L;
        testParameter[numberOfDigits+1] = 88888888888988880L;
        testParameter[numberOfDigits+2] = 12345678901234567L;                   // "+1"
        testParameter[numberOfDigits+3] = 75319753197531975L;                   // "-2 & shift"
        testParameter[numberOfDigits+4] = 52963074185296307L;                   // "-3 & shift"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 18 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith18DigitsPositive(){
        final long[] testParameter = {
            123456789987654321L,                                                // "+1"
            246802468864208642L,                                                // "+2 & shift"
            470369258852963074L,                                                // "+3 & shift"
            124716297792617421L,                                                // "+1, +2, +3, ..."
            851603565565306158L                                                 // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 18 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith18DigitsNegative(){
        final int numberOfDigits = 18;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 388888888888888888L;
        testParameter[numberOfDigits+1] = 888888888888888880L;
        testParameter[numberOfDigits+2] = 123456789012345678L;                 // "+1"
        testParameter[numberOfDigits+3] = 531975319753197531L;                 // "-2 & shift"
        testParameter[numberOfDigits+4] = 296307418529630741L;                 // "-3 & shift"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    /** Funktions-Test: "isPalindromicNumber()" 19 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith19DigitsPositive(){
        final long[] testParameter = {
            1234567890987654321L,                                              // "+1"
            7913580246420853197L,                                              // "+2 & shift"
            4703692581852963074L,                                              // "+3 & shift"
            1247162974792617421L,                                              // "+1, +2, +3, ..."
            8516035653565306158L                                               // "-3, -4, -5, ..."
        };
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  true,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindromicNumber()" 19 digits. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromicNumberWith19DigitsNegative(){
        final int numberOfDigits = 19;
        final long[] testParameter = new long[numberOfDigits+5];
        setUpArrayForPalindromicTest( testParameter, numberOfDigits, '3', '7' );
        testParameter[numberOfDigits+0] = 3888888888898888888L;
        testParameter[numberOfDigits+1] = 8888888888889888880L;
        testParameter[numberOfDigits+2] = 1234567890123456789L;                // "+1"
        testParameter[numberOfDigits+3] = 5319753197531975319L;                // "-2 & shift"
        testParameter[numberOfDigits+4] = 2963074185296307418L;                // "-3 & shift"
        //
        subTestBehaviorWithGuard_isPalindromicNumber( testParameter,  false,  1,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  combined tests
    //
    
    /** Funktions-Test: combined. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_combined_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 8235711131719232931L;
        final long expectedResultingNumber = testNumber;
        final byte[] expectedResultingArray = { 1, 3, 9, 2, 3, 2, 9, 1, 7, 1, 3, 1, 1, 1, 7, 5, 3, 2, 8 };
        final boolean expectedCheckResult = false;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertEquals(       expectedResultingNumber,    na.getNumber() );
            assertArrayEquals(  expectedResultingArray,     na.toDigitArray() );
            assertEquals(       expectedResultingNumber,    na.getNumber() );
            assertEquals(       expectedCheckResult,        na.isPalindromicNumber() );
            assertEquals(       expectedResultingNumber,    na.getNumber() );
            assertArrayEquals(  expectedResultingArray,     na.toDigitArray() );
            assertArrayEquals(  expectedResultingArray,     na.toDigitArray() );
            assertArrayEquals(  expectedResultingArray,     na.toDigitArray() );
            assertEquals(       expectedResultingNumber,    na.getNumber() );
            assertEquals(       expectedResultingNumber,    na.getNumber() );
            assertEquals(       expectedResultingNumber,    na.getNumber() );
            assertEquals(       expectedCheckResult,        na.isPalindromicNumber() );
            assertEquals(       expectedCheckResult,        na.isPalindromicNumber() );
            assertEquals(       expectedCheckResult,        na.isPalindromicNumber() );
            assertEquals(       expectedResultingNumber,    na.getNumber() );
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%d\" -> %s",  testNumber, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Stress-Test: NumberAnalyzer erzeugen  ->  -1 bzw. negativer Wert. */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_NumberAnalyzer_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long guardTestNumber = 121;
        try{
            final NumberAnalyzer_I gna = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ guardTestNumber } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long testNumber = -1;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            fail( "undetected illegal argument   ->   negative value" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: NumberAnalyzer erzeugen  ->  -42 bzw. negativer Wert. */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_NumberAnalyzer_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long guardTestNumber = 121;
        try{
            final NumberAnalyzer_I gna = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ guardTestNumber } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long testNumber = -42L;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            fail( "undetected illegal argument   ->   negative value" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: NumberAnalyzer erzeugen  ->  negativer Wert. */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_NumberAnalyzer_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long guardTestNumber = 121;
        try{
            final NumberAnalyzer_I gna = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ guardTestNumber } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long testNumber = Integer.MIN_VALUE;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            fail( "undetected illegal argument   ->   negative value" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: NumberAnalyzer erzeugen  ->  negativer Wert. */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_NumberAnalyzer_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long guardTestNumber = 121;
        try{
            final NumberAnalyzer_I gna = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ guardTestNumber } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long testNumber = Long.MIN_VALUE;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            fail( "undetected illegal argument   ->   negative value" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    /** Stress-Test: "toDigitArray()" mit 0. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_toDigitArray_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = 0L;
        final byte[] expectedResult = { 0 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: "toDigitArray()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_toDigitArray_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long testNumber = Long.MAX_VALUE;
        final byte[] expectedResult = { 7, 0, 8, 5, 7, 7, 4, 5, 8, 6, 3, 0, 2, 7, 3, 3, 2, 2, 9 };
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[]{ testNumber } ));
            assertArrayEquals( expectedResult, na.toDigitArray() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    /** Stress-Test: "isPalindromicNumber()"   ->   0. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindromicNumberWith1DigitPositive_no1(){
        subTestBehavior_isPalindromicNumber( 0L,  true,  1,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   SUPPORT
    //###
    
    private void subTestBehavior_isPalindromicNumber(
        final long testParameter,                                               // test paramter - number that shall be checked if it is a palindromic number
        final boolean expectedResult,                                           // expected result of palindrom-test
        final int points,                                                       // points in case of success
        final TL testLevel,                                                     // test level
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[] { testParameter } ));
            assertEquals( String.format( "\"%d\" -> ", testParameter ),  expectedResult, na.isPalindromicNumber() );
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%d\" -> %s",  testParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( testLevel, exercise, TC.BEHAVIOR ),  new TestResult( testName, points ) );
        }//if
    }//method()
    
    private void subTestBehaviorWithGuard_isPalindromicNumber(
        final long[] testParameter,                                             // test paramter - number(s) that shall be checked if it is a palindromic number
        final boolean expectedResult,                                           // expected result of palindrom-test
        final int points,                                                       // points in case of success
        final TL testLevel,                                                     // test level
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            subGuardTest_isPalindromicNumber();
            //\=> this execution point reached => guard test have been successfully passed
            //
            for( final long theTestParameter : testParameter ){
                final NumberAnalyzer_I na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[] { theTestParameter } ));
                assertEquals( String.format( "\"%d\" -> ", theTestParameter ),  expectedResult, na.isPalindromicNumber() );
            }//for
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%d\" -> %s",  testParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( testLevel, exercise, TC.BEHAVIOR ),  new TestResult( testName, points ) );
        }//if
    }//method()
    
    private void subGuardTest_isPalindromicNumber(){
        final String requestedRefTypeName = "NumberAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        long theGuardTestValue = 0L;
        try{
            NumberAnalyzer_I na;
            long[] guardTestValue;
            boolean expectedResult;
            //
            guardTestValue = new long[]{ 111, 222, 333, 444, 555, 666, 777, 888, 999 };     // most simple palindromic number
            theGuardTestValue = guardTestValue[ (int)( guardTestValue.length * Math.random() ) ];
            expectedResult = true;
            na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[] { theGuardTestValue } ));
            assertEquals( String.format( "Guard test failed -->> %d <<--", theGuardTestValue ),  expectedResult, na.isPalindromicNumber() );
            //
            guardTestValue = new long[]{ 789, 876, 567, 654, 345, 432, 123, 219, 891 };     // most simple non palindromic number
            theGuardTestValue = guardTestValue[ (int)( guardTestValue.length * Math.random() ) ];
            expectedResult = false;
            na = (NumberAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long.class },  new Object[] { theGuardTestValue } ));
            assertEquals( String.format( "Guard test failed -> %d", theGuardTestValue ),  expectedResult, na.isPalindromicNumber() );
            //
            // still here / this execution point reached => guard test have been successfully passed  ->  just leave method
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%d\" -> %s",  theGuardTestValue, ex.getMessage() ));
        }//try
    }//method()
    
    
    private void setUpArrayForPalindromicTest(
        final long[] testNumber,
        final int numberOfDigits,
        final char digitA,
        final char digitB
    ){
        if(( null==testNumber )  ||  ( numberOfDigits > testNumber.length ))  throw new IllegalArgumentException();
        if(( digitA < '0' )  ||  ( '9' < digitA ))  throw new IllegalArgumentException();
        if(( digitB < '0' )  ||  ( '9' < digitB ))  throw new IllegalArgumentException();
        if( digitA == digitB )                      throw new IllegalArgumentException();
        
        final char[] protoNumber = new char[ numberOfDigits ];                  // array containing digits
        final boolean middleExists = ( 0b1 == ( numberOfDigits & 0b1 ));        // numberOfDigits is odd
        final int middle = numberOfDigits >> 1;                                 // index/position in the middle
        //
        for( int indx=0; indx<numberOfDigits; indx++ ){                         // protoNumber array index running
            for( int pos=0; pos<numberOfDigits; pos++ ){                        // char array index/position running
                if( indx!=pos ){                                                // the n-th entry in protoNumber array has digitB at position n
                    protoNumber[pos] = digitA;
                }else{
                    protoNumber[pos] = digitB;
                }//if
            }//for
            //
            // construct long in a way that does NOT help the students ;-)
            String rawNumber = "";
            int i;                                                              // char array read index helping to construct number
            for( i=0; i<numberOfDigits-1; i++ ){
                rawNumber = rawNumber + protoNumber[i];
            }//for
            if( middleExists && ( indx == middle )){
                //\=> this will result in palindromic number => destroy it
                //
                rawNumber = rawNumber + '0';
            }else{
                rawNumber = rawNumber + protoNumber[i];
            }//if
            testNumber[indx] = Long.parseLong( rawNumber );
        }//for  
    }//method()
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 85;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A3;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
