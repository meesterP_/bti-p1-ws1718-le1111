package a2;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A / "1e"
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet => 0 Punkte). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );
            
            final String requestedRefTypeName = "MatrixProcessor";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};       // just something valid ;-)
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final MatrixProcessor_I matrixProcessor = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
                TS.printDetailedInfoAboutObject( matrixProcessor, "matrixProcessor" );
                //
                if( TS.isActualMethod( matrixProcessor.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  matrixProcessor.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  matrixProcessor.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "MatrixProcessor". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "MatrixProcessor". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(             classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "MatrixProcessor_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, new Class<?>[]{ long[][].class } ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, new Class<?>[]{ long[][].class }, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "MatrixProcessor" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "getMatrix",   long[][].class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "addCrossSum", long[][].class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "mirror",      long[][].class, null ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "getMatrix",   long[][].class, null, Modifier.PUBLIC ));     // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "addCrossSum", long[][].class, null, Modifier.PUBLIC ));     // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "mirror",      long[][].class, null, Modifier.PUBLIC ));     // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "MatrixProcessor" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "MatrixProcessor" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "MatrixProcessor" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: MatrixProcessor erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_MatrixProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};       // just something valid ;-)
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getMatrix_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};           // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            mp.getMatrix();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "addCrossSum()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_addCrossSum_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};           // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            mp.addCrossSum();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_mirror_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};           // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            mp.mirror();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Funktions-Test: "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getMatrix_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L }, { 3L, 4L }};                   // just something most simple and valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "addCrossSum()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_addCrossSum_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 1L },     { 1L, 1L }};
        final long[][] expectedResult = {{ 1L, 1L, 2L }, { 1L, 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.addCrossSum() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_mirror_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 1L }, { 1L, 1L }};
        final long[][] expectedResult = {{ 1L, 1L }, { 1L, 1L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    //--------------------------------------------------------------------------
    //
    //      "getMatrix()"
    //
    
    /** Funktions-Test: "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};           // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L }, { 3L, 4L }, { 5L, 6L }};       // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getMatrix()" ;  (3x). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }};           // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
            assertArrayEquals( expectedResult, mp.getMatrix() );
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getMatrix()" ;  (3x). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L, 2L }, { 3L, 4L }, { 5L, 6L }};       // just something valid ;-)
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
            assertArrayEquals( expectedResult, mp.getMatrix() );
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 1L }};
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no6(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ 0L }};
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getMatrix()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getMatrix_no7(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix = {{ Long.MIN_VALUE }};
        final long[][] expectedResult = testMatrix;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.getMatrix() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //--------------------------------------------------------------------------
    //
    //      "addCrossSum()"
    //
    
    /** Funktions-Test: "addCrossSum()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_addCrossSum_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 3L, 1L },     { 5L, 2L }};
        final long[][] expectedResult = {{ 3L, 1L, 4L }, { 5L, 2L, 7L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.addCrossSum() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "addCrossSum()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_addCrossSum_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 2L, 3L },     { 4L, 5L, 6L }};
        final long[][] expectedResult = {{ 1L, 2L, 3L, 6L }, { 4L, 5L, 6L, 15L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.addCrossSum() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "addCrossSum()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_addCrossSum_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 2L },     { 3L, 4L },     { 5L, 6L }};
        final long[][] expectedResult = {{ 1L, 2L, 3L }, { 3L, 4L, 7L }, { 5L, 6L, 11L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.addCrossSum() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "addCrossSum()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_addCrossSum_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ Long.MIN_VALUE }};
        final long[][] expectedResult = {{ Long.MIN_VALUE, Long.MIN_VALUE }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.addCrossSum() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //--------------------------------------------------------------------------
    //
    //      "mirror()"
    //
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_mirror_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 2L }};
        final long[][] expectedResult = {{ 1L }, { 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_mirror_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L }, { 2L }};
        final long[][] expectedResult = {{ 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_mirror_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 3L, 5L }, { 2L, 4L, 6L }};
        final long[][] expectedResult = {{ 1L, 2L }, { 3L, 4L }, { 5L, 6L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_mirror_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 2L }, { 3L, 4L }, { 5L, 6L }};
        final long[][] expectedResult = {{ 1L, 3L, 5L }, { 2L, 4L, 6L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_mirror_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }, { 7L, 8L, 9L }};
        final long[][] expectedResult = {{ 1L, 4L, 7L }, { 2L, 5L, 8L }, { 3L, 6L, 9L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "mirror()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_mirror_no6(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][] testMatrix     = {{ 1L, 4L, 7L }, { 2L, 5L, 8L }, { 3L, 6L, 9L }};
        final long[][] expectedResult = {{ 1L, 2L, 3L }, { 4L, 5L, 6L }, { 7L, 8L, 9L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            assertArrayEquals( expectedResult, mp.mirror() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "4s"
    //###
    
    /** MatrixProcessor erzeugen  ->  2 x 1:3 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L }, { 1L, 2L, 3L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x 2:2:1 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L, 2L }, { 1L, 2L }, { 1L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x 2:1:2 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L, 2L }, { 1L }, { 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x 1:2:2 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L }, { 1L, 2L }, { 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** MatrixProcessor erzeugen  ->  3 x 2:2:0 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no11(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L, 2L }, { 1L, 2L }, {}};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x 2:0:2 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no12(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L, 2L }, {}, { 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x 0:2:2 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no13(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{}, { 1L, 2L }, { 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  2 x 0 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no14(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{}, {}};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  1 x 0 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no15(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{}};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** MatrixProcessor erzeugen  ->  3 x 2:2:null */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no21(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L, 2L }, { 1L, 2L }, null};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x 2:null:2 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no22(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {{ 1L, 2L }, null, { 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  3 x null:2:2 */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no23(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {null, { 1L, 2L }, { 1L, 2L }};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  2 x null */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no24(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {null, null};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** MatrixProcessor erzeugen  ->  1 x null */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no25(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = {null};
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Grundsaetzlicher Test: MatrixProcessor erzeugen  ->  null */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_MatrixProcessor_no31(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "MatrixProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final long[][] guardTestMatrix = {{ 1L, 2L }, { 3L, 4L }};
        try{
            final MatrixProcessor_I gmp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ guardTestMatrix } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final long[][] testMatrix = null;
        try{
            final MatrixProcessor_I mp = (MatrixProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][].class },  new Object[]{ testMatrix } ));
            fail( "undetected illegal argument" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName,ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 46;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A2;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
