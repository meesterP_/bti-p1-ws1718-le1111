package a2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatrixProcessor implements MatrixProcessor_I {
    private long[][] originalMatrix;
    private long[][] newMatrix;

    // Constructor
    public MatrixProcessor(long[][] originalMatrix) {
        assert checkMatrix(originalMatrix) : "illegal parameter!";
        
        this.originalMatrix = originalMatrix;
        newMatrix = Arrays.copyOf(originalMatrix, originalMatrix.length);
    }

    // Methods
    @Override
    public long[][] getMatrix() {
        return originalMatrix;
    }

    @Override
    public long[][] addCrossSum() {
        long[][] crossSumMatrix = new long[newMatrix.length][];
        for (int i = 0; i < newMatrix.length; i++) {
            List<Long> twoDList = new ArrayList<Long>();
            for (int j = 0; j < newMatrix[i].length; j++) {
                twoDList.add(newMatrix[i][j]);
            }
            long sum = 0L;
            for (long e : twoDList) {
                sum += e;
            }
            twoDList.add(sum);
            crossSumMatrix[i] = new long[twoDList.size()];
            for (int j = 0; j < twoDList.size(); j++) {
                crossSumMatrix[i][j] = twoDList.get(j);
            }
        }
        return crossSumMatrix;
    }

    @Override
    public long[][] mirror() {
        int x = newMatrix.length;
        int y = newMatrix[0].length;
        
        long[][] mirroredMatrix = new long[y][];
        for (int i = 0; i < mirroredMatrix.length; i++) {
            mirroredMatrix[i] = new long[x];
        }
        
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                mirroredMatrix[i][j] = newMatrix[j][i];
            }
        }
        return mirroredMatrix;
    }
    
    private boolean checkMatrix(long[][] matrix) {
        if (matrix == null) return false;
        if (matrix.length < 1) return false;
        
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != matrix[0].length)return false;
            if (matrix[i].length < 1) return false;
            if (matrix.length < 1) return false;
        }
        return true;
    }
}
