package a2;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Das Interface MatrixProcessor_I
 * <ul>
 *     <li>beschreibt einen MatrixProcessor und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und
 *         fordert die entsprechenden Methoden ein.
 *     </li>
 * </ul>
 * Fuer diese Aufgabe sei eine Matrix "gemaess Gedankenmodell" eine echt zweidimensionales Matrix,
 * die mit einem Array long[][] modelliert wird.
 * Die von Ihnen zu implementierende Klasse MatrixProcessor muss
 * <ul>
 *     <li>einen oeffentlichen Konstruktor aufweisen, der die Matrix
 *         (<i> bzw. ein "zweidimensionales" Array vom Grund-Typ long módelliert als long[][] </i>)
 *         entgegen nimmt und daher der folgenden Signatur genuegen muss:
 *         <br />
 *         <code>MatrixProcessor( long[][] )</code>
 *     </li>
 * </ul>
 * Der Konstruktor darf nur gutartige Arrays akzeptieren.
 * <ul>
 *     <li>Vom Konstruktor akzeptierte Arrays mussen insbesondere in der zweiten Dimension
 *         eine einheitliche Anzahl von Elementen (bzw. Laenge) aufweisen.
 *     </li>
 *     <li>In jeder Dimension muessen Elemente existieren.
 *         D.h. die Anzahl der Elemente in einer beliebigen Dimension muss mindestens Eins sein.
 *     </li>
 * </ul>
 * Nicht akzeptiere Arrays sind auf die in der Vorlesung erlernte Art (Stichwort: assert) zurueckzuweisen.
 * Optional darf auch einer geeignete(!) Exception als Alternative fuer die Zurueckweisung verwendet werden.
 *<br />
 * <i>Bemerkung: Code fuer Akzeptanztests, ob eine als Parameter uebergebene Zahl geeignet ist, zaehlen zum "C-Level".</i><br />
 *<br />
 *<br />
 * Die als Parameter uebergebene Matrix wird von den nachfolgend eingeforderten Methoden benoetigt,
 * die unterschiedliche Auswertungen der Matrix implementieren und u.U. neue Matrizen als Ergebnis abliefern.
 * <br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public interface MatrixProcessor_I {
    
    /**
     * getMatrix() liefert die Matrix, so wie sie dem Konstruktor uebergeben wurde.
     *
     * @return die dem Konstruktor uebergebene Matrix.
     */
    long[][] getMatrix();
    
    
    /**
     * Fuer diese Methode soll die Matrix als ein Array ueber Zahlenkolonnen aufgefasst werden.
     * Die Methode addCrossSum() soll fuer jede Zahlenkolonne die Quersumme berechnen und
     * (in einer geeignet neu zu erzeugenden Matrix ) am Ende der jeweiligen Zahlenkolone anfuegen.
     * Die dabei neu entstandene Matrix ist als Ergebnis abzuliefern.
     *<br />
     *<br />
     *<br />
     * Beispiele:<br />
     *<br />
     * {{1,2,3},{4,5,6}} liefert {{1,2,3,6},{4,5,6,15}}<br />
     *<br />
     * {{1,2},{3,4},{5,6}} liefert {{1,2,3},{3,4,7},{5,6,11}}<br />
     *<br />
     * {{3,1},{5,2}} liefert {{3,1,4},{5,2,7}}<br />
     *<br />
     *<br />
     * @return neue Matrix, die durch das Berechnen und Anfuegen der Quersummen entsteht.
     */
    long[][] addCrossSum();
    
    
    /**
     * Diese Methode soll die beiden Dimensionen vertauschen.
     * D.h. die erste Dimension soll zur zweiten Dimension und
     * die zweite Dimension zur ersten Dimension werden.
     *<br />
     *<br />
     *<br />
     * Beispiele<br />
     *<br />
     * {{1,2,3},{4,5,6},{7,8,9}} liefert {{1,4,7},{2,5,8},{3,6,9}}<br />
     *<br />
     * {{1,4,7},{2,5,8},{3,6,9}} liefert {{1,2,3},{4,5,6},{7,8,9}}<br />
     *<br />
     *<br />
     * {{1,2},{3,4},{5,6}} liefert {{1,3,5},{2,4,6}}<br />
     *<br />
     * {{1,3,5},{2,4,6}} liefert {{1,2},{3,4},{5,6}}<br />
     *<br />
     *<br />
     * @return neue Matrix, die durch das Vertauschen der beiden Dimensionen entsteht.
     */
    long[][] mirror();
    
}//interface
