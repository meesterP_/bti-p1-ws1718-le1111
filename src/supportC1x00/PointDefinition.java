package supportC1x00;


import java.io.Serializable;


/**
 * LabExam P1<br />
 * <br />
 * ...
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/Prg_P1_LE_s16w_WI1_AfterDemo
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public class PointDefinition implements Serializable {
    
    // just for easy searches inside source code
    final static public int countsZeroPoints  =  0;                             // might be forbidden
    final static public int countsOnePoint    =  1;
    final static public int countsTwoPoints   =  2;
    final static public int countsThreePoints =  3;
    final static public int countsFourPoints  =  4;
    final static public int countsFivePoints  =  5;
    final static public int countsTenPoints   = 10;
    
    
    
    final static private long serialVersionUID = CentralVersionData.centralTestResultDataBaseRelatedSerialVersionUID;        // Note: TestResultTable is THE VERY class that is serialized
}//class
