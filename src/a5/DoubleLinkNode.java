package a5;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Just an example for a node having two links.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
class DoubleLinkNode {                  // "package scope" on purpose
    
    Data  info;                         // "package scope" on purpose
    DoubleLinkNode  prev;               // "package scope" on purpose
    DoubleLinkNode  next;               // "package scope" on purpose
    
    
    
    DoubleLinkNode ( final Data info ){ // "package scope" on purpose
        this.info = info;               // assign information object
        this.prev = null;               // currently NO predecessor existent
        this.next = null;               // currently NO successor existent
    }//constructor()
    
}//class
