package a5;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * <br />
 * <br />
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet => 0 Punkte). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "Queue";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( queue, "queue" );
                //
                if( TS.isActualMethod( queue.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  queue.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  queue.getClass().getSimpleName() );
                }//if
                //
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: Gibt es die Klasse ueberhaupt? - "Queue" */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs Queue. */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(             classUnderTest ));                  // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "Queue_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, null ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, null, Modifier.PUBLIC ));
            if( TS.deepContainsForbiddenType( requestedRefTypeWithPath, new Class<?>[]{ Collection.class, Map.class } )){
                fail( "FORBIDDEN TYPE detected" );
            }//if
        }catch( final ClassNotFoundException | TestSupportException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Queue" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "clear",   void.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "isEmpty", boolean.class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "peek",    Data.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "dequeue", Data.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "enqueue", void.class,    new Class<?>[]{ Data.class } ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "clear",   void.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "isEmpty", boolean.class, null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "peek",    Data.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "dequeue", Data.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "enqueue", void.class,    new Class<?>[]{ Data.class }, Modifier.PUBLIC )); // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Queue" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Queue" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Queue" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: Kann ueberhaupt ein Objekt erzeugt werden? - "Queue erzeugen" */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()

    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "isEmpty()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_isEmpty(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            queue.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "enqueue()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_enqueue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            queue.enqueue( anyData );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "peek()" ( und zuvor "enqueue()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequenceEnqueuePeek(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            Data justTakeData;
            queue.enqueue( anyData );
            justTakeData = queue.peek();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "dequeue()" ( und zuvor "enqueue()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequenceEnqueueDequeue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            Data justTakeData;
            queue.enqueue( anyData );
            justTakeData = queue.dequeue();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "clear()" ( und zuvor "enqueue()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_clear(){
         final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
         
         final String requestedRefTypeName = "Queue";
         final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
         try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            queue.enqueue( anyData );
            queue.clear();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    /** Einfacher Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "enqueue()", "dequeue()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = queue.isEmpty();
            queue.enqueue( anyData );
            queue.dequeue();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "enqueue()", "dequeue()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            queue.isEmpty();
            queue.enqueue( anyData );
            queue.isEmpty();
            queue.dequeue();
            queue.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "enqueue()", "peek()", "dequeue()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = queue.isEmpty();
            queue.enqueue( anyData );
            justTakeData = queue.peek();
            justTakeData = queue.dequeue();
            justTakeBool = queue.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "enqueue()", "peek()", "dequeue()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = queue.isEmpty();
            queue.enqueue( anyData );
            justTakeBool = queue.isEmpty();
            justTakeData = queue.peek();
            justTakeBool = queue.isEmpty();;
            justTakeData = queue.dequeue();
            justTakeBool = queue.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test: (NUR-)Sequenz-Methoden-Aufruf: "clear()", "enqueue()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no5(){
         final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
         
         final String requestedRefTypeName = "Queue";
         final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
         try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            //
            queue.clear();
            queue.enqueue( anyData );
            queue.clear();
            queue.enqueue( anyData );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test: (NUR-)Sequenz-Methoden-Aufruf "clear()", "dequeue()", "enqueue()", "isEmpty()", "peek()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no6(){
         final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
         
         final String requestedRefTypeName = "Queue";
         final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
         try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = queue.isEmpty();
            queue.clear();
            justTakeBool = queue.isEmpty();
            queue.enqueue( anyData );
            justTakeBool = queue.isEmpty();
            justTakeData = queue.peek();
            justTakeBool = queue.isEmpty();
            justTakeData = queue.dequeue();
            justTakeBool = queue.isEmpty();
            queue.clear();
            justTakeBool = queue.isEmpty();
            queue.enqueue( anyData );
            justTakeBool = queue.isEmpty();
            justTakeData = queue.peek();
            justTakeBool = queue.isEmpty();
            queue.clear();
            justTakeBool = queue.isEmpty();
            justTakeBool = queue.isEmpty();
            queue.enqueue( anyData );
            justTakeBool = queue.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if 
    }//method()
    
    
    /** Funktions-Test:"isEmpty()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isEmpty_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test:"isEmpty()" ( und zuvor "enqueue()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isEmpty_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 1 );
            queue.enqueue( anyData );
            assertFalse( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Funktions-Test:"isEmpty()" ( und zuvor "enqueue()" ). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isEmpty_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( queue.isEmpty() );
            //
            final Data anyData = new Data( Integer.MIN_VALUE );
            queue.enqueue( anyData );
            assertFalse( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "isEmpty()" ( und zuvor "enqueue()", "dequeue()" ). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isEmpty_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( queue.isEmpty() );
            //
            final Data anyData = new Data( 13 );
            queue.enqueue( anyData );
            assertFalse( queue.isEmpty() );
            //
            Data justTakeData = queue.dequeue();
            assertTrue( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if    
    }//method()
    
    
    /** Funktions-Test: "peek()" (und zuvor "enqueue()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_peek_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data testData = new Data( 47 );
            queue.enqueue( testData );
            assertTrue( testData == queue.peek() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "dequeue()" (und zuvor "enqueue()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_dequeue_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data testData = new Data( 53 );
            queue.enqueue( testData );
            assertTrue( testData == queue.dequeue() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "enqueue()" (und zuvor "dequeue()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_sequenceMultipleEnqueueDequeue_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=13; --i>=0; ){
                final Data testData = new Data( i );
                queue.enqueue( testData );
                assertTrue( testData == queue.dequeue() );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "clear()" (und zuvor "enqueue()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_clear_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardAnyData = new Data( 42 );
            guardQueue.enqueue( guardAnyData );
            assertFalse( guardQueue.isEmpty() );
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 11 );
            queue.enqueue( anyData );
            queue.clear();
            assertTrue( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "clear()" ("sofort"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_clear_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardAnyData = new Data( 42 );
            guardQueue.enqueue( guardAnyData );
            assertFalse( guardQueue.isEmpty() );
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            queue.clear();
            assertTrue( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test:"clear()" (in Sequenz). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_sequenceEnqueueClearIsempty_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardAnyData = new Data( 42 );
            guardQueue.enqueue( guardAnyData );
            assertFalse( guardQueue.isEmpty() );
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int queueDepth=13; --queueDepth>=0; ){
                for( int val=0; val<queueDepth; val++ ){
                    queue.enqueue( new Data( val ));
                }//for
                queue.clear();
                assertTrue( queue.isEmpty() );
            }//for
            queue.enqueue( new Data( 42 ));
            queue.clear();
            assertTrue( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
         
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()   
     
    /** Funktions-Test:"clear()" (in Sequenz). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_sequenceEnqueueClearIsempty_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int queueDepth=13; --queueDepth>=0; ){
                for( int val=0; val<queueDepth; val++ ){
                    queue.enqueue( new Data( val ));
                    assertFalse( queue.isEmpty() );
                }//for
                queue.clear();
                assertTrue( queue.isEmpty() );
            }//for
            queue.enqueue( new Data( 42 ));
            assertFalse( queue.isEmpty() );
            queue.clear();
            assertTrue( queue.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Kombinierter Test: Funktion "Queue". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_Queue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            
            for( int testRun=0; testRun<1_300; testRun+=100 ){
                int startIndexWithoutOffset = 0;
                Data dataFirst = null;
                
                assertTrue( queue.isEmpty() );
                dataFirst = null;
                startIndexWithoutOffset = 0;
                for( int i=startIndexWithoutOffset+testRun; i<42+startIndexWithoutOffset+testRun; i++ ){
                    final Data dataOrg = new Data( i );
                    if( startIndexWithoutOffset+testRun==i )  dataFirst = dataOrg;
                    queue.enqueue( dataOrg );
                    assertFalse( queue.isEmpty() );
                    assertTrue( dataFirst == queue.peek() );
                }//for
                
                //startIndexWithoutOffset = 0;   // unchanged
                for( int i=startIndexWithoutOffset+testRun; i<42+startIndexWithoutOffset+testRun; i++ ){
                    assertFalse( queue.isEmpty() );
                    final Data dataCpy = new Data( i );
                    final Data currentData = queue.peek();
                    assertTrue( currentData == queue.dequeue() );
                    assertEquals( currentData, dataCpy );
                }//for
                assertTrue( queue.isEmpty() );
                
                assertTrue( queue.isEmpty() );
                
                dataFirst = null;
                startIndexWithoutOffset = 42;
                for( int i=startIndexWithoutOffset+testRun; i<13+startIndexWithoutOffset+testRun; i++ ){
                    final Data dataOrg = new Data( i );
                    if( startIndexWithoutOffset+testRun==i )  dataFirst = dataOrg;
                    queue.enqueue( dataOrg );
                    assertFalse( queue.isEmpty() );
                    assertTrue( dataFirst == queue.peek() );
                }//for
                queue.clear();
                assertTrue( queue.isEmpty() );
                
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Stress-Test: parameter is null" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameter_null(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardQueue.enqueue( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            boolean expectedExceptionOccured = false;
            try{
                queue.enqueue( null );                                          // ATTENTION! : This will raise NO TestSupportException -> hence, do NOT check for it ;-)
                fail( "undetected illegal argument -> null accepted" );
            }catch( final AssertionError | IllegalArgumentException ex ){
                // "TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( ex );" wont work, since TestSupportException is NOT thrown
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: underflow after "dequeue()"  ->  1x"enqueue()", 2x"dequeue()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterDequeue_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardQueue.enqueue( guardSomeData );
            final Data guardJustTakeData = guardQueue.dequeue();
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            queue.enqueue( new Data( 13 ));
            final Data justTakeData = queue.dequeue();
            boolean expectedExceptionOccured = false;
            try{
                final Data notExisting = queue.dequeue();
                fail( "undetected underflow" );
            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: underflow after "peek()"  ->  1x"enqueue()", 1x"dequeue()", 1x"peek()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterPeek_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardQueue.enqueue( guardSomeData );
            final Data guardJustTakeData = guardQueue.peek();
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queueActualTest = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            queueActualTest.enqueue( new Data( 13 ));
            final Data justTakeData = queueActualTest.dequeue();
            boolean expectedExceptionOccured = false;
            try{
                final Data notExisting = queueActualTest.peek();
                fail( "undetected underflow" );
            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
         
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: underflow after "dequeue()" - idea: Nx"enqueue()", 1x"clear()", 1x"dequeue()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterDequeue_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; )  guardQueue.enqueue( new Data( (int)( 65521 *Math.random() )));
            final Data guardJustTakeData = guardQueue.dequeue();
            guardQueue.clear();
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; )  queue.enqueue( new Data( (int)( 65521 *Math.random() )));
            queue.clear();
            boolean expectedExceptionOccured = false;
            try{
                final Data notExisting = queue.dequeue();
                fail( "undetected underflow" );
            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: underflow after "peek()" - idea: Nx"enqueue()", 1x"clear()", 1x"peek)" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterPeek_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; )  guardQueue.enqueue( new Data( (int)( 65521 *Math.random() )));
            final Data guardJustTakeData = guardQueue.peek();
            guardQueue.clear();
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; )  queue.enqueue( new Data( (int)( 65521 *Math.random() )));
            queue.clear();
            boolean expectedExceptionOccured = false;
            try{
                final Data notExisting = queue.peek();
                fail( "undetected underflow" );
            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: underflow after "dequeue()"  ->  empty & 1x"dequeue()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterDequeue_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardQueue.enqueue( guardSomeData );
            final Data guardJustTakeData = guardQueue.dequeue();
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            boolean expectedExceptionOccured = false;
            try{
                final Data notExisting = queue.dequeue();
                fail( "undetected underflow" );
            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: underflow after "peek()"  ->  empty & 1x"peek()"   (no guard test) */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterPeek_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Queue_I guardQueue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardQueue.enqueue( guardSomeData );
            final Data guardJustTakeData = guardQueue.peek();
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            boolean expectedExceptionOccured = false;
            try{
                final Data notExisting = queue.peek();
                fail( "undetected underflow" );
            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Stress-Test: Ping-Pong #1. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_pingPong_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue1 = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Queue_I queue2 = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final int numberOfElements = 7;
            final int numberOfCoreTestLoopRuns = 3;
            for( int value=0; value<numberOfElements; value++ ){
                queue1.enqueue( new Data( value ));
            }//for
            
            for( int stillToDo=numberOfCoreTestLoopRuns; --stillToDo>=0; ){
                while( ! queue1.isEmpty() ){
                    queue2.enqueue( queue1.dequeue() );
                }//while
                while( ! queue2.isEmpty() ){
                    queue1.enqueue( queue2.dequeue() );
                }//while
            }//for
            
            for( int value=0; value<numberOfElements; value++ ){
                final Data expectedValue = new Data( value );
                assertEquals( queue1.dequeue(), new Data( value ) );
            }//for
            assertTrue( queue1.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    
    /** Stress-Test: Ping-Pong #2. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_pingPong_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Queue_I queue1 = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Queue_I queue2 = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final int numberOfElements = 23;
            final int numberOfCoreTestLoopRuns = 7;
            for( int value=numberOfElements; --value>=0; ){
                queue1.enqueue( new Data( value ));
            }//for
            
            for( int stillToDo=numberOfCoreTestLoopRuns; --stillToDo>=0; ){
                while( ! queue1.isEmpty() ){
                    queue2.enqueue( queue1.peek() );
                    queue2.enqueue( queue1.dequeue() );
                }//while
                while( ! queue2.isEmpty() ){
                    final Data currentData = queue2.dequeue();
                    assertEquals( currentData, queue2.dequeue() );
                    queue1.enqueue( currentData );
                }//while
            }//for
            
            for( int value=numberOfElements; --value>=0; ){
                final Data currentData = queue1.dequeue();
                assertEquals( currentData, new Data( value ));
            }//for
            assertTrue( queue1.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Kombinierter Stress-Test: Funktion "Queue". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_stressQueue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Queue";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            boolean expectedExceptionOccured = false;
            final Queue_I queue = (Queue_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            
            // keep things easy at beginning
            for( int testRun=0; testRun<19_000; testRun+=1_000 ){
                int startIndexWithoutOffset = 0;
                Data dataFirst = null;
                //
                assertTrue( queue.isEmpty() );
                //
                dataFirst = null;
                startIndexWithoutOffset = 0;
                for( int i=startIndexWithoutOffset+testRun; i<42+startIndexWithoutOffset+testRun; i++ ){
                    final Data dataOrg = new Data( i );
                    queue.enqueue( dataOrg );
                    if( startIndexWithoutOffset+testRun==i )  dataFirst = dataOrg;
                    assertFalse( queue.isEmpty() );                             // #1/2
                    assertTrue( dataFirst == queue.peek() );                    // #1/4
                    assertTrue( dataFirst == queue.peek() );                    // #2/4
                    assertTrue( dataFirst == queue.peek() );                    // #3/4
                    assertTrue( dataFirst == queue.peek() );                    // #4/4
                    assertFalse( queue.isEmpty() );                             // #2/2
                }//for
                //
                startIndexWithoutOffset = 0;
                for( int i=startIndexWithoutOffset+testRun; i<42+startIndexWithoutOffset+testRun; i++ ){
                    assertFalse( queue.isEmpty() );                             // #1/2
                    final Data dataCpy = new Data( i );
                    assertEquals( dataCpy, queue.peek() );                      // #1/4
                    assertEquals( dataCpy, queue.peek() );                      // #2/4
                    assertEquals( dataCpy, queue.peek() );                      // #3/4
                    assertEquals( dataCpy, queue.peek() );                      // #4/4
                    assertFalse( queue.isEmpty() );                             // #2/2
                    assertEquals( dataCpy, queue.dequeue() );
                }//for
                assertTrue( queue.isEmpty() );
                //
            }//for
            
            // now stress it
            for( int testRun=0; testRun<17_000_000; testRun+=1_000_000 ){
                //
                assertTrue( queue.isEmpty() );
                for( int i=0+testRun; i<42+testRun; i++ ){
                    final Data data = new Data( i );
                    queue.enqueue( data );
                    assertFalse( queue.isEmpty() );
                }//for
                
                for( int i=0+testRun; i<42+testRun; i++ ){
                    assertFalse( queue.isEmpty() );
                    final Data dataCpy = new Data( i );
                    assertEquals( dataCpy, queue.dequeue() );
                }//for
                assertTrue( queue.isEmpty() );
                
                try{
                    // causing underflow(s) by peek()
                    for( int stillToDo=3; --stillToDo>=0; ){
                        expectedExceptionOccured = false;
                        try{
                            final Data notExisting = queue.peek();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                    }//for
                    //
                    // causing underflow(s) by dequeue()
                    for( int stillToDo=3; --stillToDo>=0; ){
                        expectedExceptionOccured = false;
                        try{
                            final Data notExisting = queue.dequeue();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                    }//for
                }catch( final NullPointerException ex ){
                    fail( ex.getMessage() );
                }catch( final Exception ex ){
                    TS.printInternalNote4Exception( "queue underflow", ex, testName );
                }//try
                //
                assertTrue( queue.isEmpty() );
                
                try{
                    for( int stillToDo=3; --stillToDo>=0; ){
                        //
                        // fill queue & 1* clear() before underflow
                        for( int i=0; i<7; i++ ){
                            final Data data = new Data( i );
                            queue.enqueue( data );
                            assertFalse( queue.isEmpty() );
                        }//for
                        queue.clear();
                        //
                        // causing underflow(s) by dequeue()
                        assertTrue( queue.isEmpty() );
                        expectedExceptionOccured = false;
                        try{
                            final Data notExisting = queue.dequeue();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                        //
                        // causing underflow(s) by peek()
                        expectedExceptionOccured = false;
                        try{
                            final Data notExisting = queue.peek();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                        //
                        assertTrue( queue.isEmpty() );
                        
                        // fill queue & 1* clear() before several underflows
                        for( int i=0; i<5; i++ ){
                            final Data data = new Data( i );
                            queue.enqueue( data );
                            assertFalse( queue.isEmpty() );
                        }//for
                        queue.clear();
                        //
                        // causing underflow(s) by peek()
                        for( int i=0; i<3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                final Data notExisting = queue.peek();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        // causing underflow(s) by dequeue()
                        for( int i=0; i<3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                final Data notExisting = queue.dequeue();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        assertTrue( queue.isEmpty() );
                        
                        // n * clear() before several unflow
                        for( int i=0; i<stillToDo+1; i++ ){
                            queue.clear();
                        }//for
                        //
                        // causing underflow(s) by dequeue()
                        for( int i=0; i<stillToDo+3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                final Data notExisting = queue.dequeue();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        // causing underflow(s) by peek()
                        for( int i=0; i<stillToDo+3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                final Data notExisting = queue.peek();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | IllegalStateException | NoSuchElementException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        assertTrue( queue.isEmpty() );
                    }//for
                }catch( final NullPointerException ex ){
                    fail( ex.getMessage() );
                }catch( final Exception ex ){
                    TS.printInternalNote4Exception( "queue underflow", ex, testName );
                }//try
                assertTrue( queue.isEmpty() );
            }//for
            
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsThreePoints ));
        }//if
    }//method()        
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 41;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A5;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
