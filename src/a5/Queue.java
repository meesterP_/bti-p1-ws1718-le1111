package a5;


public class Queue implements Queue_I {
    private SingleLinkNode next;

    public Queue() {
        next = null;
    }

    @Override
    public void enqueue(Data data) {
        assert data != null : "illegal parameter!";

        SingleLinkNode node = new SingleLinkNode(data);
        
        if (next != null) {
            SingleLinkNode work = next;
            while (work.next != null) {
                work = work.next;
            }
            work.next = node;
        } else {
            next = node;
        }

    }

    @Override
    public Data dequeue() {
        if (next == null) {
            return null;
        }
        SingleLinkNode nodeToReturn = next;
        next = nodeToReturn.next;
        return nodeToReturn.info;
    }

    @Override
    public Data peek() {
        if (next == null) {
            return null;
        }
        return next.info;
    }

    @Override
    public void clear() {
        next = null;
    }

    @Override
    public boolean isEmpty() {
        return (next == null);
    }

}
