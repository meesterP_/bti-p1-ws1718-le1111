package a5;


/**
 * LabExam1111_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Das Interface Queue_I
 * <ul>
 *     <li>beschreibt einen Collector und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und
 *         fordert die entsprechenden Methoden ein.
 *     </li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse Queue muss
 * <ul>
 *     <li>sich wie eine Queue verhalten. Dies war Thema der Programmierenvorlesung.<br />
 *         Stichwort: FIFO.
 *     </li>
 *     <li>einen oeffentlichen Konstruktor aufweisen, der der folgenden Signatur genuegt:<br />
 *         <code>Queue()</code>
 *     </li>
 *     <li>eine selbstgeschriebene dynamische Datenstruktur sein.
 *         Sie duerfen fuer die Implementierung von Queue insbesondere NICHT auf die Collections zurueckgreifen.
 *         Die Implementierung muss in Form einer verketteten Liste erfolgen.
 *         Sie sollen die mitausgelieferten Klassen Data fuer Ihre Implementierung nutzen.
 *         Optional koennen Sie auf die mitausgelieferten Klassen DemoNode1L und DemoNode2L
 *         als Vorlage fuer Ihre Knoten-Klasse zurueckgreifen.
 *      </li>
 * </ul>
 * Bemerkung:<br />
 * Noch einmal: Fuer diese Aufgabe duerfen Sie NICHT auf die Collections zurueckgreifen.
 * Sie muessen die Queue mit <strong>eigenen Mitteln</strong> selbst implementieren
 * und zwar als <strong>dynamische Datenstruktur</strong> bzw. konkret als verkettete Liste.<br />
 * Um Sie von Generics zu entlasten wurde auf die Klasse Data zurueckgegriffen,
 * die Sie auch schon aus der Programmieren-Vorlesung als Platzhalter-Klasse kennen
 * (und die hier die Rolle des Generics einnimmt).<br />
 * <br />
 * 
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse
 * findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1111_4XIB1-P1_162v02_170228_v01
 */
public interface Queue_I {
    
    /**
     * Die Operation enqueue() reiht einen Datensatz (data) am Ende der Queue ein.
     * Diese Operation beachtet die LIFO-Ordnung.
     * Falls der uebergebene Parameter keine gueltige Referenz enthaelt, ist geeignet zu reagieren.
     * <br />
     * @param data bestimmt den Datensatz, der in die Queue eingereiht werden soll.
     */
    void enqueue( final Data data );
    
    /**
     * Die Operation dequeue() liefert (von den in der Queue enthaltenen Datensaetzen) den zuerst
     * in die Queue eingereihten Datensatz und entfernt ihn aus der Queue.
     * Diese Operation beachtet die LIFO-Ordnung.
     * <br />
     * @return der zuerst in die Queue eingereihte Datensatz.
     */
    Data dequeue();
    
    /**
     * Die Operation peek() liefert (von den in der Queue enthaltenen Datensaetzen) den zuerst
     * in die Queue eingereihten Datensatz. (Im Gegensatz zu dequeue() entfernt diese
     * Operation NICHT den Datensatz. Der Datensatz verbleibt in der Queue.)
     *<br />
     * @return der zuerst in die Queue eingereihte Datensatz.
     */
    Data peek();
    
    /**
     * Diese Methode leert die Queue.
     */
    void clear();
    
    /**
     * Die Operation isEmpty() prueft, ob die Queue leer ist.
     *<br />
     * @return <code>true</code> falls die Queue leer ist und
     *         sonst <code>false</code>.
     */
    boolean isEmpty();
    
}//interface
